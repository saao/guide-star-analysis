#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 11:16:24 2020

@author: freya
"""
from astropy.coordinates import SkyCoord
from astroquery.vo_conesearch import ConeSearch
from astropy.table import  setdiff , Table
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from astropy import units as u
from astroquery.vo_conesearch import conesearch

#constants

GSC='Guide Star Catalog 2.3 Cone Search 1'




"""FOV calculation"""
def fov_calculation(fov_choice):
    if fov_choice=="1":# or fov_choice==1:
        larger_fov=4*u.arcminute
        target_fov=1*u.arcminute
    elif fov_choice=="2":
        larger_fov=3*u.arcminute
        target_fov=0.5*u.arcminute
    elif fov_choice=="3":
        larger_fov=6*u.arcminute
        target_fov=1*u.arcminute
    else:
        larger_fov=1*u.arcminute
        target_fov=0.5*u.arcminute
    fov="%s' outer, %s' inner"%(str(larger_fov.value), str(target_fov))
    return larger_fov, target_fov, fov


"""Generate random targets"""
def generate_targets(number_of_targets):
    halfpi, pi, twopi = [f*np.pi for f in [0.5, 1, 2]]
#    degs, rads = 180/pi, pi/180
    ran1, ran2 = np.random.random(2*number_of_targets).reshape(2, -1)
    RA  = twopi * (ran1 - 0.5)*(180/pi)
    dec = np.arcsin(2.*(ran2-0.5)) *(180/pi)
    coords_list=SkyCoord(RA, dec, frame="icrs", unit="deg") 
    
    plt.scatter(coords_list.galactic.b, coords_list.galactic.l, s=1)
    plt.xlabel("Galactic longitude l")
    plt.ylabel("Galactic latitude b")
    plt.title("Random target coords \n for n=%s"%str(number_of_targets), fontdict = {'fontsize' : 10})
    plt.show()
    return coords_list

"Test connection"
def connection_error_test():
    error=False
    GSC='Guide Star Catalog 2.3 Cone Search 1'
    try:
        c = SkyCoord.from_name('M31')
        fix=ConeSearch.query_region(c, '0.1 deg')
        print("error if zero:",len(fix))
        result = conesearch.conesearch(c, '0.1 deg', catalog_db=GSC)
        if not result:
            print("Error connecting")
            error=True
    except:
        error=True
        print("Error connecting")
    return error


"Catalogue search with counter"
def search_GSCii(number_of_targets,larger_fov, target_fov, coords_list):
    table_guide_stars=Table()
    star_or_not=[]
    GSC='Guide Star Catalog 2.3 Cone Search 1'
    reset_count=False
    empty_count=0
    for i in range(number_of_targets): #coords_list):
        print("Target",i, "of", number_of_targets)
        c=coords_list[i]
        try:
            larger_result = conesearch.conesearch(c,0.5* larger_fov, catalog_db=GSC)
            smaller_result=conesearch.conesearch(c,0.5* target_fov, catalog_db=GSC)
#            print(larger_result)
            if not larger_result:
                print("empty")
                empty_count+=1
                star_or_not.append((coords_list[i].galactic, 0))
            if larger_result:
                star_or_not.append((coords_list[i].galactic,1))
        except:
            print("Conesearch error")
            larger_result=False
        try:
            annulus=setdiff(larger_result, smaller_result)
        except:
            print("diff error, most likely no guide stars in target fov")
            annulus=larger_result
        try:
            annulus.sort('Mag')
            brightest_guide_star=annulus[0]
            if i==0 or reset_count==True:
                table_guide_stars=Table(brightest_guide_star)
                reset_count=False
            else:
                table_guide_stars.add_row(brightest_guide_star)
                reset_count=False
        except:
#            print("No stars")
            if i==0:
                reset_count=True
#    table_guide_stars.show_in_browser()
    print(empty_count)
    return table_guide_stars, star_or_not   


def convert_results(table_guide_stars):
    important_info=table_guide_stars['Mag', 'ra', 'dec']   
    positions=SkyCoord(table_guide_stars['ra'], table_guide_stars['dec'], frame='icrs', unit="deg")
    return important_info, positions
    
    
def star_distribution(positions,table_guide_stars, number_of_targets, fov): 
    "Guide star distribution with magnitude colour scale"
    cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
    plt.scatter(positions.galactic.l, positions.galactic.b,s=1, c=table_guide_stars['Mag'],cmap=cmap)
    plt.xlabel("Galactic longitude l")
    plt.ylabel("Galactic latitude b")
    plt.colorbar(label="Magnitude")
    plt.title("Brightest guide star of a \n random distribution of targets, n=%s, fov= %s" %(str(number_of_targets), fov),fontdict = {'fontsize' : 10})
    plt.show()
    return True
    
def mag_vs_lat(positions, table_guide_stars, number_of_targets, fov):    
    "Guide star mag vs abs latitude"
    cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
    plt.scatter(abs(positions.galactic.b),table_guide_stars['Mag'], s=1,c=table_guide_stars['Mag'], cmap=cmap)
    plt.xlabel("Absolute Galactic Latitude |b|")
    plt.ylabel("Magnitude")
    plt.colorbar(label="Magnitude")
    plt.title("Brightest guide stars' magnitude \n as a function of Galactic latitude, n=%s, fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
    plt.show()
    return True

#def percentage_availability( star_or_not, number_of_targets, fov,graph="usable"):
#    "Targets lacking guide stars"
##    cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
#    abs_lat=[]
#    lat_star=[]
#    lat_no_star=[]
#    for tuples in star_or_not:
#        if tuples[1]==1:
#            lat_star.append(float(abs(tuples[0].galactic.b).value))
#        else:
#            lat_no_star.append(float(abs(tuples[0].galactic.b).value))
#        abs_lat.append(float(abs(tuples[0].galactic.b).value))
#        
#    hist_all, bins_all=np.histogram(abs_lat, bins=18, range=[0,90])
#    hist_stars, bins_stars=np.histogram(lat_star, bins=18, range=[0,90])
#    hist_no_stars, bins_no_stars=np.histogram(lat_no_star, bins=18, range=[0,90])
#    try:
#        percentage_no_stars=100*np.true_divide(hist_no_stars, hist_all)
#    except:
#        percentage_no_stars=0*hist_all
#    percentage_stars=100*np.true_divide(hist_stars, hist_all)
#    percentage_total=100*np.true_divide(hist_stars,number_of_targets )
#    return percentage_no_stars, percentage_stars, percentage_total
#    if graph=="usable":
#        print(len(lat_no_star))
#        "% per latitude bin of star and no star"
#        fig, ax=plt.subplots()
#        ax.bar(bins_all[:-1]+0.25, percentage_no_stars, 0.25, color='r', label="No stars")
#        ax.set_ylabel("% of targets")
#        ax.bar(bins_all[:-1], percentage_stars, 0.75, color='#0F52BA', label="Stars")
#        ax.set_ylim(0,110)
#        ax.legend()
#        ax.set_title("Percentage availability of guide \n stars per 5deg latitude bin,  n=%s, fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
#        ax.set_xlabel("Absolute Galactic latitude |b|")
#        plt.grid(True, 'major', 'y', ls='--', lw=.5, c='k', alpha=.3)
#        fig.tight_layout()
#        plt.show()
#
#    if graph=="all_targets":
#        "Distribution of targets by latitude bin"
#        percentage_total=100*np.true_divide(hist_stars,number_of_targets   )
#        fig, ax=plt.subplots()
#        ax.bar(bins_all[:-1], percentage_total, 0.75, color='#000080', label="% total targets")
#        ax.set_ylabel("Distribution of targets (% total)")
#        ax.set_ylim(0,110)
#        ax.legend()
#        ax.set_title("Latitude distribution of targets, \n n=%s, fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
#        ax.set_xlabel("Absolute Galactic latitude |b|")
#        plt.grid(True, 'major', 'y', ls='--', lw=.5, c='k', alpha=.3)
#        fig.tight_layout()
#        plt.show()
#    return True




"""input"""
number_of_targets=int(input("Please enter the number of targets to generate. Note approx 1s/target run time.  "))
fov_choice=input("Please input your fov annulus diameters. Type 1 for 4/1' fov, 2 for 3/0,5' fov, 3 for 6/1', anything else for tiny fov(1/0.5'):   ")

larger_fov, target_fov, fov=fov_calculation(fov_choice)
coords_list=generate_targets(number_of_targets)
error=connection_error_test()

if error:
    exit()
table_guide_stars, star_or_not=search_GSCii(number_of_targets,
                                            larger_fov, target_fov, 
                                            coords_list)
important_info, positions=convert_results(table_guide_stars)
star_distribution(positions,table_guide_stars, number_of_targets, fov)
mag_vs_lat(positions, table_guide_stars, number_of_targets, fov)
#percentage_availability(star_or_not, number_of_targets, fov)
#percentage_availability(star_or_not, number_of_targets, fov, graph="all_targets",)

#def percentage_availability( star_or_not, number_of_targets, fov,graph="usable"):
"Targets lacking guide stars"
#    cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
abs_lat=[]
lat_star=[]
lat_no_star=[]
np.errstate(invalid='ignore')
np.errstate(true_divide='ignore')
for tuples in star_or_not:
    if tuples[1]==1:
        lat_star.append(float(abs(tuples[0].galactic.b).value))
    else:
        lat_no_star.append(float(abs(tuples[0].galactic.b).value))
    abs_lat.append(float(abs(tuples[0].galactic.b).value))

hist_all, bins_all=np.histogram(abs_lat, bins=18, range=[0,90])
hist_stars, bins_stars=np.histogram(lat_star, bins=18, range=[0,90])
hist_no_stars, bins_no_stars=np.histogram(lat_no_star, bins=18, range=[0,90])
try:
    percentage_no_stars=100*np.true_divide(hist_no_stars, hist_all)
except:
    percentage_no_stars=0*hist_all
percentage_stars=100*np.true_divide(hist_stars, hist_all)
percentage_total=100*np.true_divide(hist_stars,number_of_targets )
#return percentage_no_stars, percentage_stars, percentage_total
#if graph=="usable":
#    print(len(lat_no_star))
"% per latitude bin of star and no star"
fig, ax=plt.subplots()
ax.bar(bins_all[:-1]+0.25, percentage_no_stars, 0.25, color='r', label="No stars")
ax.set_ylabel("% of targets")
ax.bar(bins_all[:-1], percentage_stars, 0.75, color='#0F52BA', label="Stars")
ax.set_ylim(0,110)
ax.legend()
ax.set_title("Percentage availability of guide \n stars per 5deg latitude bin,  n=%s, fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
ax.set_xlabel("Absolute Galactic latitude |b|")
plt.grid(True, 'major', 'y', ls='--', lw=.5, c='k', alpha=.3)
fig.tight_layout()
plt.show()

#if graph=="all_targets":
"Distribution of targets by latitude bin"
percentage_total=100*np.true_divide(hist_stars,number_of_targets   )
fig, ax=plt.subplots()
ax.bar(bins_all[:-1], percentage_total, 0.75, color='#000080', label="% total targets")
ax.set_ylabel("Distribution of targets (% total)")
ax.set_ylim(0,110)
ax.legend()
ax.set_title("Latitude distribution of targets, \n n=%s, fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
ax.set_xlabel("Absolute Galactic latitude |b|")
plt.grid(True, 'major', 'y', ls='--', lw=.5, c='k', alpha=.3)
fig.tight_layout()
plt.show()
