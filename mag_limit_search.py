#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 11:16:24 2020

@author: freya
"""
from astropy.coordinates import SkyCoord
from astroquery.vo_conesearch import ConeSearch
from astropy.table import  setdiff , Table
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from astropy import units as u
from astroquery.vo_conesearch import conesearch
from datetime import datetime


#constants

GSC='Guide Star Catalog 2.3 Cone Search 1'




"""FOV calculation"""
def fov_calculation(fov_choice, time):
    if fov_choice=="1":# or fov_choice==1:
        larger_fov=4*u.arcminute
        target_fov=1*u.arcminute
    elif fov_choice=="2":
        larger_fov=3*u.arcminute
        target_fov=0.5*u.arcminute
    elif fov_choice=="3":
        larger_fov=6*u.arcminute
        target_fov=1*u.arcminute
    else:
        larger_fov=1*u.arcminute
        target_fov=0.5*u.arcminute
    fov="%s' outer, %s' inner"%(str(larger_fov.value), str(target_fov))
    return larger_fov, target_fov, fov


"""Generate random targets"""
def generate_targets(fov_choice, time,number_of_targets):
    halfpi, pi, twopi = [f*np.pi for f in [0.5, 1, 2]]
    ran1, ran2 = np.random.random(2*number_of_targets).reshape(2, -1)
    RA  = twopi * (ran1 - 0.5)*(180/pi)
    dec = np.arcsin(2.*(ran2-0.5)) *(180/pi)
    coords_list=SkyCoord(RA, dec, frame="icrs", unit="deg") 
    plt.scatter(coords_list.galactic.b, coords_list.galactic.l, s=1)
    plt.xlabel("Galactic longitude l")
    plt.ylabel("Galactic latitude b")
    plt.title("Random target coords \n for n=%s"%str(number_of_targets), fontdict = {'fontsize' : 10})
    plt.savefig("./testing/targets_%s_%s.png"%(fov_choice, time))
    plt.show(block=False)
    plt.pause(0.5)
    plt.close()
    return coords_list

"Test connection"
def connection_error_test():
    error=False
    GSC='Guide Star Catalog 2.3 Cone Search 1'
    try:
        c = SkyCoord.from_name('M31')
        fix=ConeSearch.query_region(c, '0.1 deg')
        print("error if zero:",len(fix))
        result = conesearch.conesearch(c, '0.1 deg', catalog_db=GSC)
        if not result:
            print("Error connecting")
            error=True
    except:
        error=True
        print("Error connecting")
    return error


"Catalogue search with counter"
def search_GSCii(number_of_targets,larger_fov, target_fov, coords_list, mag_limit_list=None):
    table_guide_stars=Table()
    star_or_not=[]
    GSC='Guide Star Catalog 2.3 Cone Search 1'
    reset_count=False
    empty_count=0
    for i in range(number_of_targets): #coords_list):
        print("Target",i, "of", number_of_targets)
        c=coords_list[i]
        try:
            larger_result = conesearch.conesearch(c,0.5* larger_fov, verb=3, catalog_db=GSC)
            smaller_result=conesearch.conesearch(c,0.5* target_fov, verb=3, catalog_db=GSC)
#            print(larger_result)
            if not larger_result:
                print("empty")
                empty_count+=1
                star_or_not.append((coords_list[i].galactic, 0))
            if larger_result:
                star_or_not.append((coords_list[i].galactic,1))
        except:
            print("Conesearch error")
            larger_result=False
        try:
            annulus=setdiff(larger_result, smaller_result)
        except:
            print("diff error, most likely no guide stars in target fov")
            annulus=larger_result
        try:
            annulus.sort('Mag')
            brightest_guide_star=annulus[0]
            if i==0 or reset_count==True:
                table_guide_stars=Table(brightest_guide_star)
                reset_count=False
            else:
                table_guide_stars.add_row(brightest_guide_star)
                reset_count=False
        except:
#            print("No stars")
            if i==0:
                reset_count=True
#    table_guide_stars.show_in_browser()
    print(empty_count)
    return table_guide_stars, star_or_not   


def search_efficiently(mag_limits,number_of_targets,larger_fov, target_fov, coords_list):
#    table_guide_stars=Table()
#    star_or_not=[]
    GSC='Guide Star Catalog 2.3 Cone Search 1'
    reset_count=False
    empty_count=0
#    empty_table=Table()
    mags_stars={}
    for mag in mag_limits:
        mags_stars[str(mag)+"_star_or_not"]=[]
    for i in range(number_of_targets): #coords_list):
        print("Target",i, "of", number_of_targets)
        c=coords_list[i]
        try:
            larger_result = conesearch.conesearch(c,0.5* larger_fov, verb=3, catalog_db=GSC)
            if larger_result:
#                star_or_not.append((coords_list[i].galactic,1))
                important_info, positions=convert_results(larger_result)
                important_info.sort('Mag')
                for mag in mag_limits:
                    try_count=0
                    brightest_star=None
                    while try_count<len(important_info):#brightest_star==None:
                        try:
                            row=important_info[try_count]
                            if row['Mag']<mag:
                                sep=ang_dist(c, row['ra'], row['dec'])
                                if sep>target_fov:
                                    brightest_star=Table(row)
                            if not brightest_star:
                                try_count+=1
                        except:
                            try_count+=1
                    if brightest_star:
                        mags_stars[str(mag)+"_star_or_not"].append((coords_list[i].galactic, 1))
                        if i==0 or reset_count==True:
                            mags_stars[str(mag)]=Table(brightest_star)
#                        table_guide_stars=Table(brightest_star)
                        else:
                            
                            mags_stars[str(mag)].add_row(brightest_star)
            else:
                print("empty")
                empty_count+=1
                for mag in mag_limits:
                    mags_stars[str(mag)+"_star_or_not"].append((coords_list[i].galactic, 0))
        except:
            print("error")
            empty_count+=1
            for mag in mag_limits:
                mags_stars[str(mag)+"_star_or_not"].append((coords_list[i].galactic, 0))#        if table_guide_stars:
    return mags_stars


def convert_results(table_guide_stars):
    important_info=table_guide_stars['Mag', 'ra', 'dec']   
    positions=SkyCoord(table_guide_stars['ra'], table_guide_stars['dec'], frame='icrs', unit="deg")
    return important_info, positions

def ang_dist(target, star_ra, star_dec):
#    target=SkyCoord(target_b, target_l, unit='deg', frame='galactic')##NB
    star=SkyCoord(star_ra, star_dec, unit="deg", frame="icrs")
    sep=target.separation(star)
    return sep

def star_distribution(fov_choice,time,positions,table_guide_stars, number_of_targets, fov): 
    "Guide star distribution with magnitude colour scale"
    cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
    plt.scatter(positions.galactic.l, positions.galactic.b,s=1, c=table_guide_stars['Mag'],cmap=cmap)
    plt.xlabel("Galactic longitude l")
    plt.ylabel("Galactic latitude b")
    plt.colorbar(label="Magnitude")
    plt.title("Brightest guide star of a \n random distribution of targets, n=%s, fov= %s" %(str(number_of_targets), fov),fontdict = {'fontsize' : 10})
    plt.savefig("./testing/dist_%s_%s.png"%(fov_choice, time))
    plt.show(block=False)
    plt.pause(0.5)
    plt.close()
    return True
    
def mag_vs_lat(fov_choice,time,positions, table_guide_stars, number_of_targets, fov):    
    "Guide star mag vs abs latitude"
    cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
    plt.scatter(abs(positions.galactic.b),table_guide_stars['Mag'], s=1,c=table_guide_stars['Mag'], cmap=cmap)
    plt.xlabel("Absolute Galactic Latitude |b|")
    plt.ylabel("Magnitude")
    plt.colorbar(label="Magnitude")
    plt.title("Brightest guide stars' magnitude \n as a function of Galactic latitude, n=%s, fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
    plt.savefig("./testing/mag_%s_%s.png"%(fov_choice, time))
    plt.show(block=False)
    plt.pause(0.5)
    plt.close()
    return True



"""input"""
number_of_targets=10#int(input("Please enter the number of targets to generate. Note approx 1s/target run time.  "))
#fov_choice=input("Please input your fov annulus diameters. Type 1 for 4/1' fov, 2 for 3/0,5' fov, 3 for 6/1', anything else for tiny fov(1/0.5'):   ")
fov_list=['1', '2', '3']
fov_count=0
no_stars_list=[]

important_info_list=[]
positions_list=[]
for fov_choice in fov_list:
    now=datetime.now()
    time=now.strftime("%H:%M:%S")
    fov_count+=1
    larger_fov, target_fov, fov=fov_calculation(fov_choice,time)
    coords_list=generate_targets(fov_choice,time, number_of_targets)
    error=connection_error_test()
    if error:
        exit()
#    table_guide_stars, star_or_not=search_GSCii(number_of_targets,
#                                                larger_fov, target_fov, 
#                                                coords_list)
    mags_stars=
    important_info, positions=convert_results(table_guide_stars)
    important_info_list.append(important_info)
    positions_list.append(positions)
    star_distribution(fov_choice,time,positions,table_guide_stars, number_of_targets, fov)
    mag_vs_lat(fov_choice,time,positions, table_guide_stars, number_of_targets, fov)
    #percentage_availability(star_or_not, number_of_targets, fov)
    #percentage_availability(star_or_not, number_of_targets, fov, graph="all_targets",)
    #def percentage_availability( star_or_not, number_of_targets, fov,graph="usable"):
    "Targets lacking guide stars"
    #    cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
    abs_lat=[]
    lat_star=[]
    lat_no_star=[]
    np.errstate(invalid='ignore')
    np.errstate(true_divide='ignore')
    for tuples in star_or_not:
        if tuples[1]==1:
            lat_star.append(float(abs(tuples[0].galactic.b).value))
        else:
            lat_no_star.append(float(abs(tuples[0].galactic.b).value))
        abs_lat.append(float(abs(tuples[0].galactic.b).value))
    
    hist_all, bins_all=np.histogram(abs_lat, bins=18, range=[0,90])
    hist_stars, bins_stars=np.histogram(lat_star, bins=18, range=[0,90])
    hist_no_stars, bins_no_stars=np.histogram(lat_no_star, bins=18, range=[0,90])
    try:
        percentage_no_stars=100*np.true_divide(hist_no_stars, hist_all)
    except:
        percentage_no_stars=0*hist_all
    percentage_stars=100*np.true_divide(hist_stars, hist_all)
    percentage_total=100*np.true_divide(hist_stars,number_of_targets )
    #return percentage_no_stars, percentage_stars, percentage_total
    #if graph=="usable":
    #    print(len(lat_no_star))
    "% per latitude bin of star and no star"
    fig, ax=plt.subplots()
    ax.bar(bins_all[:-1]+0.25, percentage_no_stars, 0.25, color='r', label="No stars")
    ax.set_ylabel("% of targets")
    ax.bar(bins_all[:-1], percentage_stars, 0.75, color='#0F52BA', label="Stars")
    #ax.set_ylim(0,110)
    ax.legend()
    ax.set_title("Percentage availability of guide \n stars per 5deg latitude bin,  n=%s, fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
    ax.set_xlabel("Absolute Galactic latitude |b|")
    plt.grid(True, 'major', 'y', ls='--', lw=.5, c='k', alpha=.3)
    fig.tight_layout()
    plt.savefig("./testing/avail_%s_%s.png"%(fov_choice, time))
    plt.show(block=False)
    plt.pause(0.5)
    plt.close()
    #if graph=="all_targets":
    "Distribution of targets by latitude bin"
    percentage_total=100*np.true_divide(hist_stars,number_of_targets   )
    fig, ax=plt.subplots()
    ax.bar(bins_all[:-1], percentage_total, 0.75, color='#000080', label="% total targets")
    ax.set_ylabel("Distribution of targets (% total)")
    #ax.set_ylim(0,110)
    ax.legend()
    ax.set_title("Latitude distribution of targets, \n n=%s, fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
    ax.set_xlabel("Absolute Galactic latitude |b|")
    plt.grid(True, 'major', 'y', ls='--', lw=.5, c='k', alpha=.3)
    fig.tight_layout()
    plt.savefig("./testing/dist_2_%s_%s.png"%(fov_choice, time))
    plt.show(block=False)
    plt.pause(0.5)
    plt.close()
    no_stars_list.append(percentage_no_stars)

fig, ax=plt.subplots()
ax.bar(bins_all[:-1]+0.8, no_stars_list[0], 0.4, color='r', label="fov 4/1")
ax.bar(bins_all[:-1], no_stars_list[1], 0.4, color='b', label="fov 3/0.5")
ax.bar(bins_all[:-1]-0.8, no_stars_list[2], 0.4, color='g', label="fov 6/1")
#ax.bar(bins_all[:-1]-0.5, no_stars_list[0], 0.25, color='000080'', label="fov 4/1")

ax.set_ylabel("% of targets")
#ax.set_ylim(0,110)
ax.legend()
ax.set_title("Percentage availability of guide \n stars per 5deg latitude bin,  n=%s, fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
ax.set_xlabel("Absolute Galactic latitude |b|")
plt.grid(True, 'major', 'y', ls='--', lw=.5, c='k', alpha=.3)
fig.tight_layout()
plt.savefig("./testing/total.png")
plt.show(block=False)
plt.pause(0.5)
plt.close()
