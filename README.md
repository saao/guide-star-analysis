# Analaysis of Guide Star avaliability for MiniTracker Project

An analysis of availability of guide stars for potential SALT MiniTrackers was at completed in 2020 by [Freya Bovim](freyabovim@gmail.com) behalf of [Reta Pretorius](retha@saao.ac.za) using Python Astroquery and quearying the GSC 2.3 catalogue.

This repository contains code used and attempted, as well as some of the results in figures generated.

This code has not been organised or sanitised in any way, this is just a clone of the working repository that was used.
