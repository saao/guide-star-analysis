##!/usr/bin/env python3
## -*- coding: utf-8 -*-
#"""
#Created on Thu Feb  6 11:51:19 2020
#
#@author: freya
#"""
#
#import numpy as np
#import matplotlib.pyplot as plt
#
#from mpl_toolkits.mplot3d import Axes3D
#
#halfpi, pi, twopi = [f*np.pi for f in [0.5, 1, 2]]
#
#degs, rads = 180/pi, pi/180
#
## do radians first, then convert later
#
#nstars = 1000
#
#ran1, ran2 = np.random.random(2*nstars).reshape(2, -1)
#
#RA  = twopi * (ran1 - 0.5)
#dec = np.arcsin(2.*(ran2-0.5)) # Hey Barry Carter!
#
#
#
#
#funcs = [np.cos, np.sin]
#
#cosRA,  sinRA  = [f(RA)  for f in funcs]
#cosdec, sindec = [f(dec) for f in funcs]
#
#x = cosRA * cosdec
#y = sinRA * cosdec
#z = sindec
#
#decs = (np.arange(11)-5) * halfpi / 6.
#RAs  = (np.arange(12)-5) * halfpi / 6.
#
#
#
#theta = np.linspace(0, twopi, 101)
#
#costh, sinth = [f(theta) for f in funcs]
#zerth = np.zeros_like(theta)
#
#fig = plt.figure()
#
#ax  = fig.add_subplot(1, 1, 1, projection='3d')
#
#ax.plot(x, y, z, '.k')
#
## lines of declination
#xvals = costh * np.cos(decs)[:, None]
#yvals = sinth * np.cos(decs)[:, None]
#zvals = zerth + np.sin(decs)[:, None]
#for x, y, z in zip(xvals, yvals, zvals):
#    plt.plot(x, y, z, '-g', linewidth=0.8)
#
## lines of Right Ascention
#xvals = costh * np.cos(RAs)[:, None]
#yvals = costh * np.sin(RAs)[:, None]
#zvals = sinth + np.zeros_like(RAs)[:, None]
#for x, y, z in zip(xvals, yvals, zvals):
#    plt.plot(x, y, z, '-r', linewidth=0.8)
#
#ax.set_xlim(-1.1, 1.1)
#ax.set_ylim(-1.1, 1.1)
#ax.set_zlim(-1.1, 1.1)
#
#ax.view_init(elev=30, azim=15)
#
#plt.show()
#
#if True:
#    plt.figure()
#    plt.plot(degs*RA, degs*dec, '.k')
#    plt.xlim(-180, 180)
#    plt.ylim(-90, 90)
#    plt.show()

import numpy as np
from astropy.coordinates import SkyCoord
from astropy import units as u
import matplotlib.pyplot as plt

def random_point_on_unit_sphere():
    while True:
        R   = np.random.rand(3) #Random point in box
        R   = 2*R - 1
        rsq = sum(R**2)
        if rsq < 1: break       #Use r only if |r|<1 in order not to favor corners of box
    return R / np.sqrt(rsq)     #Normalize to unit vector

def random_point_on_sky():
    p     = random_point_on_unit_sphere()
    r     = np.linalg.norm(p)
    theta = 90 - (np.arccos(p[2] / r)    / np.pi * 180)            #theta and phi values in degrees
    phi   =       np.arctan(p[1] / p[0]) / np.pi * 180
    c     = SkyCoord(ra=phi, dec=theta, unit=(u.degree, u.degree)) #Create coordinate
    return c.ra.deg, c.dec.deg                                     #Many different formats are possible, e.g c.ra.hour for decimal hour values

def print_random_star_coords(nstars):
    ra=[]
    Dec=[]
    for n in range(nstars):
        RA,dec = random_point_on_sky()
        ra.append(RA)
        Dec.append(dec)
    return (ra, Dec)
RA, dec = print_random_star_coords(100)
print(RA, dec)
plt.scatter(RA, dec)
plt.show()