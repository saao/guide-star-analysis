#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 11:16:24 2020

@author: freya
"""
from astropy.coordinates import SkyCoord
from astroquery.vo_conesearch import ConeSearch
from astropy.table import  Table
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from astropy import units as u
from astroquery.vo_conesearch import conesearch
from datetime import datetime


#constants

GSC='Guide Star Catalog 2.3 Cone Search 1'




"""FOV calculation"""
def fov_calculation(fov_choice):
    if fov_choice=="1":# or fov_choice==1:
        larger_fov=4*u.arcminute
        target_fov=1*u.arcminute
    elif fov_choice=="2":
        larger_fov=3*u.arcminute
        target_fov=0.5*u.arcminute
    elif fov_choice=="3":
        larger_fov=6*u.arcminute
        target_fov=1*u.arcminute
    else:
        larger_fov=2*u.arcminute
        target_fov=0.5*u.arcminute
    fov="%s' outer, %s' inner"%(str(larger_fov.value), str(target_fov))
    return larger_fov, target_fov, fov


"""Generate random targets"""
def generate_targets(fov_choice, time,number_of_targets):
    halfpi, pi, twopi = [f*np.pi for f in [0.5, 1, 2]]
    ran1, ran2 = np.random.random(2*number_of_targets).reshape(2, -1)
    RA  = twopi * (ran1 - 0.5)*(180/pi)
    dec = np.arcsin(2.*(ran2-0.5)) *(180/pi)
    coords_list=SkyCoord(RA, dec, frame="icrs", unit="deg") 
#    plt.scatter(coords_list.galactic.b, coords_list.galactic.l, s=1)
#    plt.xlabel("Galactic longitude l")
#    plt.ylabel("Galactic latitude b")
#    plt.title("Random target coords \n for n=%s"%str(number_of_targets), fontdict = {'fontsize' : 10})
#    plt.savefig("targets_%s_%s.png"%(fov_choice, time))
#    plt.show(block=False)
#    plt.pause(0.5)
#    plt.close()
    return coords_list

"Test connection"
def connection_error_test():
    error=False
    GSC='Guide Star Catalog 2.3 Cone Search 1'
    try:
        c = SkyCoord.from_name('M31')
        fix=ConeSearch.query_region(c, '0.1 deg')
        print("error if zero:",len(fix))
        result = conesearch.conesearch(c, '0.1 deg', catalog_db=GSC)
        if not result:
            print("Error connecting")
            error=True
    except:
        error=True
        print("Error connecting")
    return error

def ang_dist(target, star_ra, star_dec):
#    target=SkyCoord(target_b, target_l, unit='deg', frame='galactic')##NB
    star=SkyCoord(star_ra, star_dec, unit="deg", frame="icrs")
    sep=target.separation(star)
    return sep

"Catalogue search with counter"
def search_GSCii(number_of_targets,larger_fov, target_fov, coords_list):
    table_guide_stars=Table()
    star_or_not=[]
    GSC='Guide Star Catalog 2.3 Cone Search 1'
    reset_count=False
    empty_count=0
    for i in range(number_of_targets): #coords_list):
        print("Target",i, "of", number_of_targets)
        c=coords_list[i]
        try:
            larger_result = conesearch.conesearch(c,0.5* larger_fov, catalog_db=GSC)
#            smaller_result=conesearch.conesearch(c,0.5* target_fov, catalog_db=GSC)
#            print(larger_result)
            annulus=larger_result
            if not larger_result:
                print("empty")
                empty_count+=1
                star_or_not.append((coords_list[i].galactic, 0))
#            if larger_result:
        except:
            print("Conesearch error")
            exit()
            larger_result=False
            empty_count+=1
#        try:
         #setdiff(larger_result, smaller_result)
#        except:
#            print("diff error, most likely no guide stars in target fov")
#            annulus=larger_result
        try:
            brightest_guide_star=False
            annulus.sort('Mag')
            for j in range(len(annulus)):
                sep= ang_dist(c, annulus[j]['ra'], annulus[j]['dec'])
                if sep>target_fov:
                    brightest_guide_star=annulus[j]
                    print("inside annulus")
                    break
                else:
                    print("inside target field")
#            brightest_guide_star=annulus[0]
            if brightest_guide_star:
                star_or_not.append((coords_list[i].galactic,1))
                if i==0 or reset_count==True:
                    table_guide_stars=Table(brightest_guide_star)
                    reset_count=False
                else:
                    table_guide_stars.add_row(brightest_guide_star)
                    reset_count=False
            else:
                star_or_not.append((coords_list[i].galactic, 0))
                empty_count+=1
        except:
            if i==0:
                reset_count=True
    print(empty_count)
    return table_guide_stars, star_or_not   


def convert_results(table_guide_stars):
    important_info=table_guide_stars['Mag', 'ra', 'dec']   
    positions=SkyCoord(table_guide_stars['ra'], table_guide_stars['dec'], frame='icrs', unit="deg")
    return important_info, positions
    
    
def star_distribution(fov_choice,time,positions,table_guide_stars, number_of_targets, fov): 
    "Guide star distribution with magnitude colour scale"
    cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
    plt.scatter(positions.galactic.l, positions.galactic.b,s=1, c=table_guide_stars['Mag'],cmap=cmap)
    plt.xlabel("Galactic longitude l")
    plt.ylabel("Galactic latitude b")
    plt.colorbar(label="Magnitude")
    plt.title("Brightest guide star of a \n random distribution of targets, n=%s, fov= %s" %(str(number_of_targets), fov),fontdict = {'fontsize' : 10})
    plt.savefig("dist_%s_%s.png"%(fov_choice, time))
    plt.show(block=False)
    plt.pause(0.5)
    plt.close()
    return True
    
def mag_vs_lat(fov_choice,time,positions, table_guide_stars, number_of_targets, fov):    
    "Guide star mag vs abs latitude"
    cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
    plt.scatter(abs(positions.galactic.b),table_guide_stars['Mag'], s=1,c=table_guide_stars['Mag'], cmap=cmap)
    plt.xlabel("Absolute Galactic Latitude |b|")
    plt.ylabel("Magnitude")
    plt.colorbar(label="Magnitude")
    plt.title("Brightest guide stars' magnitude \n as a function of Galactic latitude, n=%s, fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
    plt.savefig("mag_%s_%s.png"%(fov_choice, time))
    plt.show(block=False)
    plt.pause(0.5)
    plt.close()
    return True

def mag_limited(mag_limit_list, important_info,star_or_not):
    mag_dict={}
    not_star=[]
    for star in star_or_not:
        if star[1]==0:
            not_star.append(star)
#            print("not star appended")
    for mag in mag_limit_list:
        mag_dict[str(mag)]=[]
    for star in important_info:
        star_mag=star['Mag']
        star_coords=SkyCoord(star['ra'], star['dec'], unit="deg", frame="icrs")
        for mag in mag_limit_list:
            if star_mag> mag:
#                print("cut of by mag", mag)
                mag_dict[str(mag)].append((star_coords.galactic,0))
            else:
#                print("not cut off by mag", mag)
                mag_dict[str(mag)].append((star_coords.galactic,1))
    return mag_dict, not_star



def run_func(number_of_targets,fov_list,mag_limit_list):
#fov_count=0
#    no_stars_list=[]
    list_of_table_guide_star=[]
    list_of_star_or_not=[]
    for i in range(len(fov_list)):
        fov_choice=fov_list[i]
        now=datetime.now()
        time=now.strftime("%H:%M:%S")
    #    fov_count+=1
        larger_fov, target_fov, fov=fov_calculation(fov_choice,time)
        coords_list=generate_targets(fov_choice,time, number_of_targets)
        error=connection_error_test()
        if error:
            exit()
        table_guide_stars, star_or_not=search_GSCii(number_of_targets,
                                                    larger_fov, target_fov, 
                                                    coords_list)
        list_of_table_guide_star.append(table_guide_stars)
        list_of_star_or_not.append(star_or_not)
    return list_of_table_guide_star,list_of_star_or_not, time, fov



def show_plots(mag_dict, mag_limit_list,time, fov_choice,fov, not_star):
    mag_dict_other={}
    for mag in mag_limit_list: 
        mag_dict_other[str(mag)]=[]
        abs_lat=[]
        lat_star=[]
        lat_no_star=[]
        np.errstate(invalid='ignore')
        np.errstate(true_divide='ignore')
        for tuples in mag_dict[str(mag)]:
            if tuples[1]==1:
                lat_star.append(float(abs(tuples[0].galactic.b).value))
            else:
                lat_no_star.append(float(abs(tuples[0].galactic.b).value))
            abs_lat.append(float(abs(tuples[0].galactic.b).value))
        
        hist_all, bins_all=np.histogram(abs_lat, bins=18, range=[0,90])
        hist_stars, bins_stars=np.histogram(lat_star, bins=18, range=[0,90])
        hist_no_stars, bins_no_stars=np.histogram(lat_no_star, bins=18, range=[0,90])
        try:
            percentage_no_stars=100*np.true_divide(hist_no_stars, hist_all)
        except:
            percentage_no_stars=0*hist_all
        percentage_stars=100*np.true_divide(hist_stars, hist_all)
        percentage_total=100*np.true_divide(hist_stars,number_of_targets )
        mag_dict_other[str(mag)].append(percentage_no_stars)
        mag_dict_other[str(mag)].append(percentage_stars)
        mag_dict_other[str(mag)].append(percentage_total)
        mag_dict_other[str(mag)].append(bins_all)
    if len(not_star)!=0:
        mag_dict_other["3"]=[]
        abs_lat=[]
        lat_star=[]
        lat_no_star=[]
        np.errstate(invalid='ignore')
        np.errstate(true_divide='ignore')
        for tuples in not_star:
            if tuples[1]==1:
                lat_star.append(float(abs(tuples[0].galactic.b).value))
            else:
                lat_no_star.append(float(abs(tuples[0].galactic.b).value))
            abs_lat.append(float(abs(tuples[0].galactic.b).value))
        
        hist_all, bins_all=np.histogram(abs_lat, bins=18, range=[0,90])
        hist_stars, bins_stars=np.histogram(lat_star, bins=18, range=[0,90])
        hist_no_stars, bins_no_stars=np.histogram(lat_no_star, bins=18, range=[0,90])
        try:
            percentage_no_stars=100*np.true_divide(hist_no_stars, hist_all)
        except:
            percentage_no_stars=0*hist_all
        percentage_stars=100*np.true_divide(hist_stars, hist_all)
        percentage_total=100*np.true_divide(hist_stars,number_of_targets )
        mag_dict_other["3"].append(percentage_no_stars)
        mag_dict_other["3"].append(percentage_stars)
        mag_dict_other["3"].append(percentage_total)
        mag_dict_other["3"].append(bins_all)
    return mag_dict_other
    #if graph=="usable":
    #    print(len(lat_no_star))
    
    
def mag_vs_lat_2(dict_tables, mag_limit_list,fov_list):#,time,positions, table_guide_stars, number_of_targets, fov):    
    "Guide star mag vs abs latitude"
    cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
    fig, (ax1,ax2,ax3)=plt.subplots(3,1, sharex=True,figsize=(6,9))
    axes=(ax1,ax2,ax3)
    fig.subplots_adjust(hspace=0.2)
    for i in range(len(dict_tables)):
        fov_choice=fov_list[i]
        table_guide_stars=dict_tables[fov_choice][0]
        positions=dict_tables[fov_choice][1]
        a,b,fov=fov_calculation(fov_choice)
        ax=axes[i]
        ax.scatter(abs(positions.galactic.b),table_guide_stars['Mag'], s=1,c=table_guide_stars['Mag'], cmap=cmap)
        ax.set_ylabel("Magnitude")
        ax.set_ylim(4,21)
        ax.axhline(y=15, color='r', ls='--')
        ax.axhline(y=17, color='b', ls='--')
        ax.axhline(y=18, color='g', ls='--')
        if ax==ax3:
            ax.set_xlabel("Absolute Galactic Latitude |b|")
        if ax==ax1:
            ax.set_title("Brightest guide stars' magnitude by Galactic latitude, n=%s \n fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
        else:
            ax.set_title("fov= %s" %fov, fontdict = {'fontsize' : 10})    
        ax.grid(True, 'major', 'y', ls='--', lw=.5, c='k', alpha=.3)
        ax.set_yticks(np.arange(4, 21, 2))
    fig.subplots_adjust(right=0.8)
#    fig.colorbar(c_ax=ax1)#label="Magnitude",cax=cbar_ax)
    plt.savefig("comparison_g_stars.png")
    plt.show(block=False)
    plt.pause(0.5)
    plt.close()
    return True

def create_plots(dict_of_dicts, mag_limit_list,fov_list):
    "% per latitude bin of star and no star"
    fig, (ax1,ax2,ax3)=plt.subplots(3,1, sharex=True,figsize=(6,9))#, sharey=True)
    axes=(ax1,ax2,ax3)
    fig.subplots_adjust(hspace=0.2)
    for i in range(len(dict_of_dicts)):
        fov_choice=fov_list[i]
        a,b,fov=fov_calculation(fov_choice)
        ax=axes[i]
        bins_all=dict_of_dicts[fov_choice][str(mag_limit_list[0])][3]
        ax.bar(bins_all[:-1]+0.8, dict_of_dicts[fov_choice][str(mag_limit_list[0])][0], 0.75, color='r', label="mag {}".format(str(mag_limit_list[0])))
        ax.set_ylabel("% ")
        ax.bar(bins_all[:-1], dict_of_dicts[fov_choice][str(mag_limit_list[1])][0], 0.75, color='b', label="mag {}".format(str(mag_limit_list[1])))
        ax.bar(bins_all[:-1]+1.6, dict_of_dicts[fov_choice][str(mag_limit_list[2])][0], 0.75, color='g', label="mag {}".format(str(mag_limit_list[2])))
        if len(not_star)!=0:
            ax.bar(bins_all[:-1]+3.2, dict_of_dicts[fov_choice][str(3)][0], 0.75, color='y', label="No stars at target")
        ax.set_ylim(0,110)
        if ax==ax1:
            ax.legend(title_fontsize='8')
            ax.set_title("Percentage of Guide Stars dimmer than mag limit, per 5deg latitude bin,  n=%s, \n fov= %s" %(str(number_of_targets), fov), fontdict = {'fontsize' : 10})
        else:
            ax.set_title("fov= %s" % fov, fontdict = {'fontsize' : 10})
        if ax==ax3:
            ax.set_xlabel("Absolute Galactic latitude |b|")
        ax.grid(True, 'major', 'y', ls='--', lw=.5, c='k', alpha=.3)
        ax.set_xticks(np.arange(0, 91, 5))
    fig.tight_layout()
    plt.savefig("mag_limits.png)
    plt.show(block=False)
    plt.pause(1)
    plt.close()
  

"""input"""
number_of_targets=int(input("Please enter the number of targets to generate. Note approx 1s/target run time.  "))
#fov_choice=input("Please input your fov annulus diameters. Type 1 for 4/1' fov, 2 for 3/0,5' fov, 3 for 6/1', anything else for tiny fov(1/0.5'):   ")
fov_list=['1', '2', '3']#,'4']
mag_limit_list=[15,17,18]

list_of_table_guide_star,list_of_star_or_not, time, fov=run_func(number_of_targets,fov_list,mag_limit_list)
dict_of_dicts={}
dict_tables={}
for i in range(len(fov_list)):
#    dict_of_dicts[fov_list[i]]=[]
    star_or_not=list_of_star_or_not[i]
    table_guide_stars=list_of_table_guide_star[i]
    fov_choice=fov_list[i]   
    if len(table_guide_stars)!=0:
        important_info, positions=convert_results(table_guide_stars)
        mag_dict, not_star=mag_limited(mag_limit_list, important_info,star_or_not)
        mag_dict_other=show_plots(mag_dict, mag_limit_list,time, fov_choice,fov, not_star)
        dict_of_dicts[fov_choice]=mag_dict_other
        dict_tables[fov_choice]=[important_info, positions]
    else:
        print("Fov returned no stars anywhere")

create_plots(dict_of_dicts, mag_limit_list,fov_list)
mag_vs_lat_2(dict_tables, mag_limit_list,fov_list)